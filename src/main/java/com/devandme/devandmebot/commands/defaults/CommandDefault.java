package com.devandme.devandmebot.commands.defaults;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.core.DevAndMe;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class CommandDefault {
	
	private final DevAndMe dam;
	
	public CommandDefault(DevAndMe dam) {
		this.dam = dam;
	}
	
	@Command(name="stop", type= ExecutorType.CONSOLE)
	private void stop(){
		dam.stop();
	}
	
	@Command(name="help")
	private void help(User user, MessageChannel channel, Message message, String[] args){
		channel.sendMessage("*Aucune commande à afficher pour le moment...*").queue();
	}
}