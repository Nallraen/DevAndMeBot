package com.devandme.devandmebot.commands;

import com.devandme.devandmebot.core.DevAndMe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public final class PowerRegistry {

    private static final Path PATH = Paths.get("power-registry.json");

    private static final Logger LOGGER = LoggerFactory.getLogger(PowerRegistry.class);

    private static PowerRegistry ourInstance = new PowerRegistry();

    private final Map<Long, Integer> powers = new HashMap<>();

    private PowerRegistry() {
        this.load();
    }

    public synchronized void load() {
        LOGGER.debug("Chargement des power...");

        if (Files.exists(PATH)) {
            try (BufferedReader reader = Files.newBufferedReader(PATH)) {

                this.powers.clear();

                for (Power power : DevAndMe.GSON.fromJson(reader, Power[].class)) {
                    this.powers.put(power.userId, power.value);
                }

                LOGGER.debug("{} power ont été chargés.", this.powers.size());
            }
            catch (IOException e) {
                LOGGER.error("Impossible de charger la liste des power!", e);
            }
        }
        else {
            LOGGER.debug("Aucun power chargé, le fichier est inexistant.");
        }
    }

    public synchronized void save() {
        LOGGER.debug("Sauvegarde des power...");
        
        try (BufferedWriter writer = Files.newBufferedWriter(PATH, StandardCharsets.UTF_8)) {

            Power[] powers = this.powers.entrySet().stream().map(Power::new).toArray(Power[]::new);

            DevAndMe.GSON.toJson(powers, writer);
        }
        catch (IOException e) {
            LOGGER.error("Impossible de charger la liste des power!", e);
        }

        LOGGER.debug("{} power ont été sauvegardés.", this.powers.size());
    }

    public Integer getPower(long userId) {
        return this.powers.get(userId);
    }

    public void setPower(long userId, int power) {
        this.powers.put(userId, power);
    }
    
    public void remove(long userId){
    	this.powers.remove(userId);
    }


    public static PowerRegistry getInstance() {
        return ourInstance;
    }

    private final class Power {

        public long userId;

        public int value;

        @SuppressWarnings("unused")
		public Power() { }

        public Power(Map.Entry<Long, Integer> entry) {
            this.userId = entry.getKey();
            this.value = entry.getValue();
        }
    }
}
