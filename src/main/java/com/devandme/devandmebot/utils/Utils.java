package com.devandme.devandmebot.utils;

public abstract class Utils {
	
	public static String formatInteger(int i){
		if(i/1000 >= 1){
			return String.format("%.1f", (double)i/1000)+"k";
		}
		
		return Integer.toString(i);
	}

}
