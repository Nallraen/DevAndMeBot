package com.devandme.devandmebot.listeners;

import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.core.DataRegistery;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.entities.impl.UserImpl;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.EventListener;

public class BotListener implements EventListener {
	
private final CommandMap commandmap;
	
	public BotListener(CommandMap commandmap) {
		this.commandmap = commandmap;
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof MessageReceivedEvent) onMessage((MessageReceivedEvent)event);
		else if(event instanceof GuildMemberJoinEvent) onJoin((GuildMemberJoinEvent)event);
	}

	private void onJoin(GuildMemberJoinEvent event) {
		TextChannel joinedchannel = event.getGuild().getDefaultChannel();
		User user = event.getUser();
		TextChannel ctm = event.getJDA().getTextChannelsByName("bienvenue", true).get(0);
		joinedchannel.sendMessage(
				"Bienvenue sur le Discord 'Dev & Me', "+user.getAsMention()+" !\n\nJe me présente, je suis le bot officiel du serveur. Je suis là pour t'accompagner et t'aider en cas de besoin ! Tout d'abord, je t'invite à lire les règles qui se trouvent dans "
				+ctm.getAsMention()+
				". Ensuite, sache que si tu veut intéragir avec moi, tu peux grâce à la commande **!help** *(qui est à faire dans le salon 'spam-et-bot')*. D'ailleurs, pour éviter d'être trop déranger, je t'invite à mettre ce salon en sourdire !\n\nAmuse-toi bien sur Dev & Me !").queue();
	}

	private void onMessage(MessageReceivedEvent event) {
		if(event.getAuthor().equals(event.getJDA().getSelfUser())) return;
		if(!(event.getChannel() instanceof TextChannel)) return;
		
		TextChannel tc = event.getTextChannel();
		User user = event.getAuthor();
		Member m = event.getMember();
		String message = event.getMessage().getContent();
		
		//Add message to count
		new DataRegistery().addMessage();
		
		//Only ONE MESSAGE IS ALLOW ON THIS CHANNEL !
		if(tc.getName().equalsIgnoreCase("presentation")){
			tc.createPermissionOverride(m).setDeny(Permission.MESSAGE_WRITE).queue();
			return;
		}
		
		if(message.startsWith(commandmap.getTag())){
			//Bon salon
			if(tc.getName().equalsIgnoreCase("bot-et-spam")){
				message = message.replaceFirst(commandmap.getTag(), "");
				commandmap.commandUser(event.getAuthor(), message, event.getMessage());
			}
			//Mauvais salon
			else{
				//On ouvre le channel privé si il n'existe pas encore
				if(!user.hasPrivateChannel()){
					try{
						user.openPrivateChannel().complete();
					}catch(Exception e){}
				}
				//on lui envoye le message
				try{
					((UserImpl)user).getPrivateChannel().sendMessage("[Dev&Me] Merci d'utiliser le salon 'bot-et-spam' pour les commandes !").queue();
				}catch(Exception e){}
			}
			//Supprimer le message
			if((event.getTextChannel() != null) && (event.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_MANAGE))){
				event.getMessage().delete().queue();
			}
		}
	
	}

}
